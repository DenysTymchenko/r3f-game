import { useEffect, useRef } from 'react';
import { controller } from '../../../utils/api.js';
import useGame from '../../../store/useGame.jsx';
import StyledButton from './StyledButton.jsx';
import './startMenu.css'

export default function StartMenu({ setPlay }) {
  const { playerName, setPlayerName, toggleMenuIsOpened } = useGame((state) => state);
  const changePlayerNameInput = useRef();
  const inviteBtnMsg = useRef();

  const createUser = async () => {
    const users = await controller('GET', 'Users');
    const generatedUserName = `Player${users.length + 1}`;
    setPlayerName(generatedUserName);
    controller('POST', 'Users', { 'name': generatedUserName });
  };

  async function changePlayerName(newPlayerName) {
    const users = await controller('GET', 'Users');
    const usernameAlreadyExists = users.find(user => user.name === newPlayerName);

    if (!usernameAlreadyExists) {
      const userData = users.find(user => user.name === playerName);
      controller('PUT', `Users/${userData.id}`, {
        'name': newPlayerName,
        'id': userData.id
      });

      const highScores = await controller('GET', 'Highscores');
      const highScoresUser = highScores.find(user => user.name === playerName);
      if (highScoresUser) {
        controller('PUT', `Highscores/${highScoresUser.id}`, {
          'name': newPlayerName,
          'score': highScoresUser.score,
          'id': highScoresUser.id
        });
      }

      setPlayerName(newPlayerName);
    } else {
      const playerNameP = document.querySelector('.buttons .btn:nth-child(2) .text');
      playerNameP.innerText = localStorage.getItem('playerName');
    }
  };

  const openChangePlayerNameInput = () => {
    const playerNameP = document.querySelector('.buttons .btn:nth-child(2) .text')
    playerNameP.style.display = 'none'
    changePlayerNameInput.current.style.display = 'block'
    changePlayerNameInput.current.value = playerName;
    changePlayerNameInput.current.focus();
  }

  const showInviteMessage = () => {
    inviteBtnMsg.current.classList.add('show');
    inviteBtnMsg.current.onanimationend = (e) => e.target.classList.remove('show');
  }

  if (!playerName) createUser();

  useEffect(() => {
    changePlayerNameInput.current.addEventListener("focusout", () => {
      const newPlayerName = changePlayerNameInput.current.value;

      const playerNameP = document.querySelector('.buttons .btn:nth-child(2) .text')
      playerNameP.innerText = newPlayerName
      changePlayerNameInput.current.style.display = 'none'
      playerNameP.style.display = 'block'

      changePlayerName(newPlayerName);
    });
  }, [playerName, changePlayerNameInput])


  return (
    <div className='start-menu'>
      <div className='logo'>
        <img src='./startMenu/logo.svg' alt='shield logo' />
        <h1>Castle Run</h1>
      </div>

      <div className='buttons'>
        <StyledButton text={'PLAY'} event={() => {
          setPlay(true)
          toggleMenuIsOpened()
        }} />
        <StyledButton text={playerName} event={() => openChangePlayerNameInput()}>
          <input type='text' ref={changePlayerNameInput}/>
        </StyledButton>
        <StyledButton text={'Invite Friends'} event={() => showInviteMessage()} />
      </div>
      <p className='invite-btn-msg' ref={inviteBtnMsg}>Link copied</p>
    </div>
  )
}
